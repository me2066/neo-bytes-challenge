#to install os
FROM ubuntu

#to install apache2 package
RUN apt-get install apache2

#to copy file from base machine
COPY tesfile /tmp

#to unzip and add the file
ADD test.tar.tz /tmp
